import React, { useState, useEffect, useCallback } from 'react'
import { Redirect, useParams } from "react-router-dom"
import TreeComponent from '../../components/TreeComponents/TreeComponent'
import "./Dashboard.css"
import Save from "../../assets/shape/save.svg"
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import AddIcon from '@material-ui/icons/Add';
import GraphIcon from "../../assets/graph/add-graphs.png"
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';
import FlowChart from '../../components/FlowChart/FlowChart'
import { AppContext } from '../../utils/AppContext'
import axios from "../../utils/Axios"
import ShowSelected from '../../components/ShowSelected/ShowSelected'
import nodes from "../../utils/ExtraNodes"
import ResultModal from '../../components/Modals/ResultModal'
import { ReactFlowProvider } from 'react-flow-renderer';
import Modal from './Modal'
import HomeIcon from '@material-ui/icons/Home';
import ClearIcon from '@material-ui/icons/Clear';
import { useMutation, useQuery } from '@apollo/client'
import { GET_FLOW } from '../../GraphQL/queries'
import { SAVE_FLOW } from '../../GraphQL/mutations'
import { cloneDeep } from 'lodash';
import Loader from '../../components/Loader/Loader'
import { useAuth0 } from "@auth0/auth0-react";


const Dashboard = (props) => {

    const { id } = useParams()

    const { isAuthenticated, getAccessTokenSilently, user } = useAuth0();

    // console.log(id)

    // GQL
    const { loading: getFlowLoading, refetch: getFlow } = useQuery(GET_FLOW, { variables: { id: id } })
    const [saveFlow, { loading: saveFlowLoading }] = useMutation(SAVE_FLOW);


    const allFlows = JSON.parse(localStorage.getItem('curFlows'))
    const curFlow = allFlows.filter(a => a.uuid === id)[0]

    const [openLeft, setOpenLeft] = useState(true)
    const [openRight, setOpenRight] = useState(true)
    const [files, setFiles] = useState({})
    const [docName, setDocName] = useState('')
    const [elements, setElements] = useState([])
    const [selected, setSelected] = useState([])
    const [collection, setCollection] = useState([])
    const [run, setRun] = useState(false)
    const [rfInstance, setRfInstance] = useState(null);
    const [show, setShow] = useState(false)
    const [response, setResponse] = useState({})
    const [wait, setWait] = useState(false)
    const [ontype, setONtype] = useState('')
    const [group, setGroup] = useState([])
    const [tbId, setTbId] = useState('')
    const [resize, setResize] = useState(false)
    const [view, setView] = useState(true)
    const [dsModal, setDSModal] = useState(false)
    const [active, setActive] = useState(0)
    const [tabs, setTabs] = useState([curFlow])
    const [selectable, setSelectable] = useState(true)
    const [token, setToken] = useState('')

    // For showing output modal

    const [showModal, setShowModal] = useState(false)


    useEffect(() => {
        const getToken = async () => {
            const domain = process.env.REACT_APP_AUTH0_DOMAIN;

            try {
                console.log("Entered try block...")
                const accessToken = await getAccessTokenSilently({
                    audience: `https://${domain}/api/v2/`,
                    scope: 'openid profile email offline_access'
                });
                setToken(accessToken)
            } catch (e) {
                console.log(e.message);
            }
        };
        if (user && token==='') {
            console.log("Entered")
            getToken()
        }
    }, [user])

    const handleCollapseLeft = () => {
        if (openLeft) { setOpenLeft(false) }
        else { setOpenLeft(true) }
    }

    const handleCollapseRight = () => {
        if (openRight) { setOpenRight(false) }
        else { setOpenRight(true) }
    }

    const gotoHome = () => {
        props.history.push('/nodles')
    }
    const activateTab = (index, flow) => {
        setActive(index)
        props.history.push(`/nodles/flow/${flow.uuid}`)
    }

    const addTabs = (tab) => {
        let newTabs = tabs
        newTabs.push(tab)
        setTabs(newTabs)
        setActive(newTabs.length - 1)
    }

    const deleteTabs = (index) => {
        let curId = id;
        let newTabs = tabs.filter((t, i) => i !== index)
        setTabs(newTabs)
        props.history.push(`/nodles/flow/${curId}`)
    }


    //  To get the content of tree structure

    useEffect(() => {
        axios.get('/tasks', {
            headers: {
                'Access-Control-Allow-Headers': true
            }
        })
            .then(res => {
                const fetchedFiles = res.data
                nodes.forEach(n => {
                    fetchedFiles.collections.unshift(n)
                })
                setFiles(fetchedFiles)
            })
    }, [])

    //  To get the all the flows

    useEffect(() => {
        axios.get('/get_task_list')
            .then(res => {
            })
    }, [])

    //  To get the elements from localstorage


    // For changing document title

    useEffect(() => {
        if (isAuthenticated) {
            setDocName(curFlow.name)
        }

    }, [curFlow, isAuthenticated])

    useEffect(() => {
        if (isAuthenticated) {
            document.title = `Nodles - ${docName}`
        }

    }, [docName, isAuthenticated])

    useEffect(() => {
        getFlow()
            .then((res) => {
                const flowData = cloneDeep(res.data.nodles_flow_runs[0]);
                if (flowData.flow === '') {
                    setElements([]);
                } else {
                    setElements(flowData.flow.elements);
                }

            })
    }, [id])
    // Resetting everything

    const handleReset = () => {
        let res = window.confirm("All nodes will be lost. Want to continue?")
        if (res) {
            setElements([])
            setRfInstance(null)
            localStorage.removeItem('elements')
        }
    }

    // To run the animation

    const handleRun = () => {
        if (rfInstance) {
            // let doc = docName.replace(' ', '-').toLowerCase()
            console.log("Running")

            const flow = rfInstance.toObject();
            localStorage.setItem('elements', JSON.stringify(flow));
            localStorage.setItem('doc', JSON.stringify(docName))
            // props.history.push(`/nodles/flow/${doc}`)

            setRun(true)
            setWait(true)
            const JSON_DATA = JSON.stringify(flow)

            axios.post('/run_task',
                JSON_DATA,
                {
                    headers: {
                        "Access-Control-Allow-Origin": "*"
                    }
                })
                .then(res => {
                    setWait(false)
                    setResponse(res.data)
                    setShow(true)
                    setRun(false)
                })

            setTimeout(() => {
                setShow(false)
            }, 4000)
        }
    }


    // To stop the animation


    const handleStop = () => {
        setRun(false)
    }


    // Saving the state of nodes and edges in playground

    const saveState = useCallback(() => {
        if (rfInstance) {
            let doc = docName.replace(' ', '-').toLowerCase()

            const flow = rfInstance.toObject();
            localStorage.setItem('elements', JSON.stringify(flow));
            localStorage.setItem('doc', JSON.stringify(docName))
            // props.history.push(`/nodles/flow/${doc}`)

            const savedData = {
                name: doc,
                elements: flow.elements,
                position: flow.position
            }
            saveFlow({ variables: { flow: savedData, id: id } });
            axios.post('/save_task', savedData)
                .then(res => res)
        }
    }, [rfInstance, docName, id]);

    const values = {
        elements,
        setElements,
        rfInstance,
        setRfInstance,
        setSelected,
        selected,
        run,
        setRun,
        showModal,
        setShowModal,
        response,
        wait,
        setONtype,
        group,
        setGroup,
        setTbId,
        resize,
        setResize,
        collection,
        setCollection,
        selectable,
        setSelectable,
        token,
        setToken,
        saveState
    }

    if (!isAuthenticated) {
        return <Redirect to="/nodles" />
    }
    else {

        return (
            <AppContext.Provider value={values}>
                {
                    (saveFlowLoading || getFlowLoading)
                    &&
                    <Loader />
                }
                {dsModal && (
                    <Modal
                        allFlows={allFlows}
                        view={view}
                        setView={setView}
                        setDSModal={setDSModal}
                        urlLoc={props}
                        addTabs={addTabs} />
                )}
                <div className="container dashboard">

                    <div className="split-containers">
                        <div className="side-navbar">
                            <div className="home-icon">
                                <HomeIcon onClick={gotoHome} />
                            </div>
                            <div className="add-icon">
                                <AddIcon className="svg-icon" onClick={() => setDSModal(true)} />
                            </div>
                            <div className="icons save" onClick={saveState}>
                                <img src={Save} alt="" />
                                <div className="info-box save">Save Playground</div>
                            </div>
                            <button className="reset-btn" onClick={handleReset}>
                                <RotateLeftIcon className="svg-icon" />
                            </button>
                            <div className="graph-icon">
                                <img src={GraphIcon} alt="" />
                            </div>
                        </div>

                        <div className="others">

                            <div className="top-navbar">

                                <div className="tabs">
                                    {
                                        tabs.map((t, i) => (
                                            <div className={`tab ${active === i ? 'active' : ''}`}>
                                                <div className="docName" onClick={() => activateTab(i, t)}>
                                                    <p>{t.name}</p>
                                                </div>
                                                <ClearIcon onClick={() => deleteTabs(i)} className="svg-icon" />
                                            </div>
                                        ))
                                    }

                                    <AddIcon onClick={() => setDSModal(true)} className="add-tabs-icon" />
                                </div>



                                <div className="controls">
                                    <p className={`success ${show ? 'show' : ''}`}>Output evaluated.</p>
                                    <div className="icons start" onClick={handleRun} style={{ cursor: `${!run ? 'pointer' : 'not-allowed'}` }}>
                                        <PlayArrowIcon className="svg-icon" />
                                        <div className="tooltip start">
                                            Start
                                        </div>
                                    </div>
                                    <div className="icons stop" onClick={handleStop} style={{ cursor: `${run ? 'pointer' : 'not-allowed'}` }}>
                                        <StopIcon className="svg-icon" />
                                        <div className="tooltip stop">
                                            Stop
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {tabs.length ? (
                                <div className="workspace">
                                    <div className={`tree-container ${!openLeft ? 'collapse' : ''}`}>
                                        <div className={`sub-container ${!openLeft ? 'hide' : ''}`}>
                                            {Object.keys(files).length !== 0 && <TreeComponent files={files} />}
                                        </div>

                                        <button className="collapse-btn"
                                            onClick={handleCollapseLeft}>
                                            {openLeft ? (
                                                <KeyboardArrowLeftIcon className="svg-icon" />
                                            ) : (
                                                <KeyboardArrowRightIcon className="svg-icon" />
                                            )}
                                        </button>
                                    </div>

                                    <div className={`flow-chart-field ${!openLeft && !openRight ? 'extraBroaden' :
                                        !openRight || !openLeft ? 'broaden' :
                                            ''}`}>
                                        {run && <div className="layer"></div>}

                                        {/* Showing table modal here for CSS issues */}
                                        {showModal && ontype === 'table' ?
                                            <ResultModal
                                                setShowModal={setShowModal}
                                                data={response}
                                                type={ontype}
                                                id={tbId}
                                            />
                                            : showModal && ontype === 'chart' ? (
                                                <ResultModal setShowModal={setShowModal} data={response} type={ontype} id={tbId} />

                                            )
                                                : (
                                                    ''
                                                )
                                        }


                                        <div className="work-field">
                                            <ReactFlowProvider>
                                                <FlowChart />
                                            </ReactFlowProvider>

                                        </div>

                                    </div>



                                    <div className={`ref-field ${!openRight ? 'collapse' : ''}`}>
                                        <button className="collapse-btn"
                                            onClick={handleCollapseRight}>
                                            {openRight ? (
                                                <KeyboardArrowRightIcon className="svg-icon" />
                                            ) : (
                                                <KeyboardArrowLeftIcon className="svg-icon" />
                                            )}
                                        </button>
                                        <div className={`sub-container ${!openRight ? 'hide' : ''}`}>

                                            {openRight && <ShowSelected />}

                                            <div className="divider"></div>
                                        </div>

                                    </div>
                                </div>
                            ) : (
                                <div className="no-flows">
                                    <p>No Flows Selected</p>
                                </div>
                            )}

                        </div>
                    </div>
                </div>
            </AppContext.Provider>
        )
    }
}

export default Dashboard
