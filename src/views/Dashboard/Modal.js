import React from 'react'
import GridSVG from "../../assets/grid/gridIcon.svg"
import ListSVG from "../../assets/list/listIcon.svg"
import GridSelected from "../../assets/grid/gridSelected.svg"
import ListSelected from "../../assets/list/listSelected.svg"
import Card from '../Home/Card'
import List from '../Home/List'
import CancelIcon from '@material-ui/icons/Cancel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const Modal = (props) => {

    const { setView, allFlows, view, setDSModal, urlLoc, addTabs } = props

    return (
        <div className="allFlows-modal">
            <div className="modal-box">
                <CancelIcon onClick={() => setDSModal(false)} className="close-icon" />
                <h1>Open Flow</h1>
                <br />
                <br />
                <div className="search-box">
                    <input type="search" placeholder="Search" />
                </div>

                <br />

                <div className="filters">
                    <div className="select-order">
                        <p style={{ whiteSpace: 'nowrap' }}>Last Edited</p>
                        <span><ExpandMoreIcon className="down-icon" /></span>
                    </div>
                    <div style={{ display: 'flex', columnGap: '10px' }}>
                        <div className="grid filter-icons" onClick={() => setView(true)}>
                            {!view ? (
                                <img src={GridSVG} alt="" />
                            ) : <img src={GridSelected} alt="" />}
                        </div>
                        <div className="list filter-icons" onClick={() => setView(false)}>
                            {view ? (
                                <img src={ListSVG} alt="" />
                            ) : <img src={ListSelected} alt="" />}
                        </div>
                    </div>
                </div>

                {
                    view && allFlows.length ? (
                        <div className="grid flows">
                            <div className="cards">
                                {allFlows.map((f) => (
                                    <Card
                                        key={f.uuid}
                                        path={urlLoc}
                                        flow={f}
                                        flows={allFlows}
                                        setDM={setDSModal}
                                        addTabs={addTabs}
                                    />
                                ))}
                            </div>
                        </div>
                    ) : allFlows.length ? (
                        <div className="list flows">
                            <table className="lists">
                                <tr>
                                    <th>File Name</th>
                                    <th>Last Edited</th>
                                    <th>Tags</th>
                                </tr>


                                {allFlows.map(f => (

                                    <List
                                        path={urlLoc}
                                        key={f.uuid}
                                        flow={f}
                                        flows={allFlows}
                                        setDM={setDSModal}
                                        addTabs={addTabs}
                                    />
                                ))}
                            </table>
                        </div>
                    ) : (
                        <p className="no-flows">No Flows</p>
                    )
                }
            </div>
        </div>
    )
}

export default Modal
