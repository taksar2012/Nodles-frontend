import React from 'react'
import { useAuth0 } from "@auth0/auth0-react";

const LoginButton = () => {

    const { loginWithRedirect } = useAuth0();

    return (
        <div>
            <button onClick={(e) => {
                e.preventDefault()
                loginWithRedirect()
                }
            } className="auth-btn login">Log In</button>
        </div>
    )
}

export default LoginButton
