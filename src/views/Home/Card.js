import React, { useState, useRef } from 'react'
import Sample from "../../assets/sample.png"
import MenuSVG from "../../assets/menu/menu.svg"
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { getFormattedDate } from '../../utils/commonFunctions';

const Card = (props) => {

    const ctrlBtn = useRef()

    const { path, index, flow, onDelete, onRename, onDuplicate, flows, setDM, addTabs } = props
    const [ctrlMenu, setCtrlMenu] = useState(false)
    const [title, setTitle] = useState(flow.name)
    const [edit, setEdit] = useState(false)
    // const [tags, setTags] = useState([])
    // const [tag, setTag] = useState('')
    // const [modal, setModal] = useState(false)

    const openFlow = () => {
        path.history.push(`/nodles/flow/${flow.uuid}`)
        setCtrlMenu(false)
        localStorage.setItem('curFlows', JSON.stringify(flows))
        if (typeof setDM !== "undefined") {
            setDM(false)
        }
        if (typeof addTabs !== "undefined") {
            addTabs(flow)
        }
    }

    const handleDelete = () => {
        onDelete(flow.uuid, index);
        setCtrlMenu(false)
    }

    const handleDuplicate = () => {
        onDuplicate(flow.uuid, index);
        setCtrlMenu(false)
    }

    const handleRename = () => {
        if (title !== '') {
            onRename(flow.uuid, title, index);
            setEdit(false);
        }

    }

    window.addEventListener('click', (e) => {
        let x = ctrlBtn.current
        if (x !== e.target) {
            setCtrlMenu(false)
        }
    })

    // const addTag = () => {
    //     if (tag !== '' && tag.length <= 16) {
    //         let newTags = tags
    //         newTags.push(tag)
    //         setTags(newTags)
    //         setModal(false)
    //         setTag('')
    //     }
    //     else {
    //         alert("Tag must be within 0-16 characters.")
    //     }
    // }

    return (
        <div className="card">
            {/* {modal && (
                <div className="add-tag-modal">
                    <div className="modal-box">
                        <h3>Flow Tags:</h3>
                        <CancelIcon className="close-icon" onClick={() => setModal(false)} />
                        <div className="tag-grid">
                            {tags.map(t => (
                                <p>{t}</p>
                            ))
                            }
                        </div>
                        <div className="add-tag-input">
                            <input onChange={(e) => setTag(e.target.value)} value={tag}></input>
                            <button onClick={addTag}>Add</button>
                        </div>
                    </div>
                </div>
            )} */}
            <div className="preview" onClick={openFlow}>
                <img src={Sample} alt="" />
            </div>
            <div className="about">
                <div className="details">
                    <div className="ip-cont">
                        <input className={`title ${edit ? 'edit' : ''}`}
                            readOnly={!edit}
                            onChange={(e) => setTitle(e.target.value)} value={title} />
                        {edit && <CheckCircleOutlineIcon onClick={handleRename} className="tick-icon" />}
                    </div>
                    <p>Last edited : {getFormattedDate(new Date(flow.updated_at).getTime())}</p>
                </div>
                {
                    !path.location.pathname.includes('flow') && (
                        <div className="controls">
                            <img src={MenuSVG} alt="" />
                            <div id="control-btn" ref={ctrlBtn} onClick={() => setCtrlMenu(!ctrlMenu)}></div>
                            {ctrlMenu && (
                                <div className="control-menu">
                                    <ul>
                                        <li onClick={openFlow}>Open</li>
                                        <li onClick={handleDuplicate}>Duplicate</li>
                                        <li onClick={() => { setEdit(true) }}>Rename</li>
                                        <li >Add Tag</li>
                                        <li onClick={handleDelete}>Delete</li>
                                    </ul>
                                </div>
                            )}
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default Card
