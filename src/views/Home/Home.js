import React, { useState, useEffect } from 'react'
import "./Home.css"
import Logo from "../../assets/logo/logo.png"
import Path from "../../assets/folder/Path.svg"
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Workspace from './Workspace'
import { GET_ALL_WORKSPACES } from '../../GraphQL/queries';
import { useQuery, useMutation } from '@apollo/client'
import { CREATE_WORKSPACE, DELETE_WORKSPACE, RENAME_WORKSPACE } from '../../GraphQL/mutations'
import Loader from '../../components/Loader/Loader'
import { cloneDeep } from '@apollo/client/utilities';
import { useAuth0 } from "@auth0/auth0-react";
import LoginButton from './Auth/LoginButton';
import LogoutButton from './Auth/LogoutButton';
import SignupButton from './Auth/SignupButton';


const Home = (props) => {

    const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();

    const [workspaces, setWorkspaces] = useState([])
    const [openLeft, setOpenLeft] = useState(true);
    const [selectedWorkspaceIndex, setSelectedWorkspaceIndex] = useState(0);
    const { loadng: workspaceLoading, data: workspaceData, refetch: fetchAllWorkSpace } = useQuery(GET_ALL_WORKSPACES);
    const [createWorkspace, { loading: createWorkspaceLoading }] = useMutation(CREATE_WORKSPACE);
    const [deleteWorkspace, { loading: deleteWorkspaceLoading }] = useMutation(DELETE_WORKSPACE);
    const [renameWorkspace, { loading: renameWorkspaceLoading }] = useMutation(RENAME_WORKSPACE);

    useEffect(() => {

        workspaceData && setWorkspaces(workspaceData.nodles_workspace || []);
        // setWorkspaces(workspaceData.nodles_workspace);

        return () => {

        }
    }, [workspaceData]);


    useEffect(() => {
        workspaces.length !== 0 && setSelectedWorkspaceIndex(0);
    }, [workspaces.length]);


    const addWorkspace = () => {
        createWorkspace({ variables: { name: 'New Workspace - ' + (workspaces.length + 1) } })
            .then(() => {
                fetchAllWorkSpace();
            });
        // setWorkspaces(w => [...w, newSpace])
    }

    const onDeleteWorkspace = (workspaceId, index) => {
        deleteWorkspace({ variables: { w_id: workspaceId } })
            .then(() => {
                setSelectedWorkspaceIndex(0);
                const newWorkspaces = cloneDeep(workspaces);
                newWorkspaces.splice(index, 1);
                setWorkspaces(newWorkspaces);
            })
    }

    const onRenameWorkspace = (workspaceId = '', newName = '', index) => {
        renameWorkspace({ variables: { w_id: workspaceId, name: newName } })
            .then(() => {
                const newWorkspaces = [
                    ...workspaces
                ]
                newWorkspaces[index] = {
                    ...newWorkspaces[index],
                    name: newName
                }
                setWorkspaces(newWorkspaces)
            })
    }

    const handleCollapseLeft = () => {
        if (openLeft) { setOpenLeft(false) }
        else { setOpenLeft(true) }
    }

    return (
        <div className="home container">
            {
                (workspaceLoading || createWorkspaceLoading || deleteWorkspaceLoading || renameWorkspaceLoading)
                &&
                <Loader />
            }
            <div className="top-navbar">
                <div className="brand">
                    <img src={Logo} alt="logo" />
                </div>
                <div className="auth-details">
                    {isAuthenticated && (
                        <div>{user.name}</div>
                    )}
                    {!isAuthenticated ? (
                        <div style={{ display: 'flex', alignItems: 'center', columnGap: '16px' }}>
                            <SignupButton />
                            <LoginButton />
                        </div>
                    ) : <LogoutButton />}
                </div>
            </div>

            {isAuthenticated ? (
                <div className="split-containers">

                    <div className={`workspace-container ${!openLeft ? 'collapse' : ''}`}>
                        <button className="collapse-btn"
                            onClick={handleCollapseLeft}>
                            {openLeft ? (
                                <KeyboardArrowLeftIcon className="svg-icon" />
                            ) : (
                                <KeyboardArrowRightIcon className="svg-icon" />
                            )}
                        </button>
                        <div className="sub-wkp-container">
                            <ul>
                                {workspaces.length ? workspaces.map((w, index) => (
                                    <li className={`${selectedWorkspaceIndex === index ? 'active' : ''}`}
                                        onClick={() => setSelectedWorkspaceIndex(index)} key={w.uuid}>
                                        <img src={Path} alt="path" style={{ marginRight: '8px' }} />
                                        <span>{w.name}</span>
                                    </li>
                                )) : <li className="no-wkp">No workspaces</li>}
                            </ul>

                            <div className="divider"></div>

                            <button className="add-btn" onClick={addWorkspace}>+  &nbsp;New workspace</button>
                        </div>

                    </div>

                    <div className={`playgrounds ${!openLeft ? 'expand' : ''}`}>

                        {workspaces.length ? (
                            <Workspace
                                urlLoc={props}
                                index={selectedWorkspaceIndex}
                                workspace={workspaces[selectedWorkspaceIndex]}
                                onDeleteWorkspace={onDeleteWorkspace}
                                onRenameWorkspace={onRenameWorkspace}
                            />
                        ) :
                            <div className="create-wkp">
                                <p>No workspace created</p>
                                <button onClick={addWorkspace}>Create Workspace Now</button>

                            </div>
                        }

                    </div>
                </div>
            ) : (
                <div className="welcome-container">
                    <h1>Welcome</h1>
                </div>
            )}


        </div>
    )
}

export default Home
