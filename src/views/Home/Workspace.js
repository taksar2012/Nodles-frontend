import React, { useState, useEffect } from 'react'
import Card from './Card';
import List from './List'
import GridSVG from "../../assets/grid/gridIcon.svg"
import ListSVG from "../../assets/list/listIcon.svg"
import GridSelected from "../../assets/grid/gridSelected.svg"
import ListSelected from "../../assets/list/listSelected.svg"
import EditIcon from '@material-ui/icons/Edit';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { useMutation, useQuery } from '@apollo/client';
import { DELETE_FLOW, CREATE_FLOW, RENAME_FLOW } from '../../GraphQL/mutations';
import { GET_WORKSPACE_FLOW } from '../../GraphQL/queries';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { DeleteForeverRounded } from '@material-ui/icons';
import Loader from '../../components/Loader/Loader';
import { cloneDeep } from '@apollo/client/utilities';

const Workspace = (props) => {

    const { urlLoc, index, workspace, onDeleteWorkspace, onRenameWorkspace } = props

    // const [flows, setFlows] = useState([], )
    const [view, setView] = useState(true)
    const [wkpEdit, setwkpEdit] = useState(false)
    const [wkpTitle, setwkpTitle] = useState(workspace.name)
    const [flows, setFlows] = useState([]);

    const { data: rawFlowData, loading: getFlowLoading, refetch: getAllFlows } = useQuery(GET_WORKSPACE_FLOW, { variables: { w_id: workspace.uuid } });
    const [handleFlowDelete, { loading: deleteFlowLoading }] = useMutation(DELETE_FLOW);
    const [handleAddFlow, { loading: addFlowLoading }] = useMutation(CREATE_FLOW);
    const [handleRenameFlow, { loading: renameFlowLoading }] = useMutation(RENAME_FLOW);
    const addFlow = () => {
        handleAddFlow({ variables: { w_id: workspace.uuid, name: 'New Flow: ' + (flows.length + 1), flow: "" } })
            .then(() => {
                getAllFlows();
            })
        // setFlows(f => [...f, newFlow])
    }


    const onFlowDelete = (id = '', index) => {
        handleFlowDelete({ variables: { id: id } })
            .then(() => {
                const newFlows = cloneDeep(flows);
                newFlows.splice(index, 1);
                setFlows(newFlows);
            })
            .catch(() => {

            })
    }
    const onFlowDuplicate = (id = '') => {

    }
    const onFlowRename = (id = '', newName = '', index) => {

        handleRenameFlow({ variables: { id: id, name: newName } })
            .then(() => {
                const newFlows = cloneDeep(flows);
                newFlows[index]['name'] = newName;
                setFlows(newFlows);
            })
            .catch(() => {

            })

    }

    useEffect(() => {
        rawFlowData && rawFlowData.nodles_workspace[0] && setFlows(rawFlowData.nodles_workspace[0].nodles_flow_runs);
    }, [rawFlowData])
    useEffect(() => {
        setwkpTitle(workspace.name);
    }, [workspace.name])

    return (
        <>
            {
                (getFlowLoading || addFlowLoading || deleteFlowLoading || renameFlowLoading)
                &&
                <Loader />
            }
            <div className="header">

                <div className="wkp-title">
                    <div className="edit-wkp-title">
                        <input className={`wkpTitle ${wkpEdit ? 'edit' : ''}`}
                            readOnly={!wkpEdit}
                            onChange={(e) => setwkpTitle(e.target.value)} value={wkpTitle} />
                        {wkpEdit ?
                            <CheckCircleOutlineIcon onClick={() => {
                                onRenameWorkspace(workspace.uuid, wkpTitle, index)
                                setwkpEdit(false);
                            }} className="tick-icon" />
                            : <EditIcon onClick={() => setwkpEdit(true)} className="edit-icon" />}

                        <DeleteForeverRounded color={'error'} className={'delete-icon'} onClick={() => onDeleteWorkspace(workspace.uuid, index)} />

                    </div>
                </div>

                <button onClick={addFlow}>+ New Flow</button>
            </div>

            <div className="options">
                <div className="tags-container">
                    {/* <span>
                        Tag name 1
                                    </span>
                    <span>
                        Tag name 1
                                    </span>
                    <span>
                        Tag name 1
                                    </span>
                    <span>
                        Tag name 1
                                    </span>
                    <span>
                        Tag name 1
                                    </span> */}
                </div>

                <div className="filters">
                    <div className="select-order">
                        <p style={{ whiteSpace: 'nowrap' }}>Last edited</p>
                        <span><ExpandMoreIcon className="down-icon" /></span>
                    </div>
                    <div className="grid filter-icons" onClick={() => {setView(true); console.log("fsdfffsd")}}>
                        {!view ? (
                            <img src={GridSVG} alt="" />
                        ) : <img src={GridSelected} alt="" />}
                    </div>
                    <div className="list filter-icons" onClick={() => {setView(false); console.log("bdfbd")}}>
                        {view ? (
                            <img src={ListSVG} alt="" />
                        ) : <img src={ListSelected} alt="" />}
                    </div>
                </div>
            </div>

            {
                view && flows.length ? (
                    <div className="grid flows">
                        <div className="cards">
                            {flows.map((f, index) => (
                                <Card
                                    key={f.uuid}
                                    index={index}
                                    path={urlLoc}
                                    flow={f}
                                    onDelete={onFlowDelete}
                                    onRename={onFlowRename}
                                    onDuplicate={onFlowDuplicate}
                                    flows={flows}
                                />
                            ))}
                        </div>
                    </div>
                ) : flows.length ? (
                    <div className="list flows">
                        <table className="lists">
                            <tr>
                                <th>File Name</th>
                                <th>Last Edited</th>
                                <th>Tags</th>
                            </tr>


                            {flows.map(f => (

                                <List
                                    path={urlLoc}
                                    key={f.uuid}
                                    flow={f}
                                    onDelete={onFlowDelete}
                                    onRename={onFlowRename}
                                    onDuplicate={onFlowDuplicate}
                                    flows={flows}
                                />
                            ))}
                        </table>
                    </div>
                ) : (
                    <p className="no-flows">No Flows</p>
                )
            }
        </>
    )
}

export default Workspace
