import { gql } from '@apollo/client';

export const CREATE_WORKSPACE = gql `
    mutation createWorkspace($name: String!) {
        insert_nodles_workspace_one(object: {name: $name}) {
            uuid,	created_at, name, description,    updated_at
        }
    }
`

export const RENAME_WORKSPACE = gql`
  mutation renameWorkspace($w_id: uuid!, $name: String!){
    update_nodles_workspace(_set: {name: $name}, where: {uuid: {_eq: $w_id}}){
     affected_rows   
    }
  }
`

export const DELETE_WORKSPACE = gql `
    mutation deleteWorkspace($w_id: uuid!) {
        delete_nodles_workspace(where: {uuid: {_eq: $w_id}}){
            affected_rows
        }
    }
`

export const CREATE_FLOW = gql `
    mutation createFlow($flow: jsonb!,$name: String!,$w_id: uuid!) {
        insert_nodles_flow_runs_one(object: {flow: $flow, name: $name, workspace_uuid: $w_id}) {
            uuid, created_at, name, updated_at
        }
    }
`

export const DELETE_FLOW = gql`
    mutation deleteFlow($id: uuid!) {
    delete_nodles_flow_runs_by_pk(uuid: $id) {
      created_at
      name
      updated_at
    }
  }
`

export const RENAME_FLOW = gql`
    mutation renameFlow($id: uuid!, $name: String!){
        update_nodles_flow_runs(_set: {name: $name}, where: {uuid: {_eq: $id}}){
            affected_rows
        }
        
    }
`

export const SAVE_FLOW = gql`
    mutation saveFlow($id: uuid!, $flow: jsonb!){
        update_nodles_flow_runs(_set: {flow: $flow}, where: {uuid: {_eq: $id}}){
            affected_rows
        }
    }
`