import { gql } from '@apollo/client';
export const GET_ALL_WORKSPACES = gql`
    query  {
        nodles_workspace {
        created_at
        description
        name
        updated_at
        uuid
        }
    }
`

export const GET_WORKSPACE_FLOW = gql `
    query getWorkspaceFlows($w_id: uuid!){
        nodles_workspace(where: {uuid: {_eq: $w_id}}) {
            nodles_flow_runs {
                created_at
                name
                updated_at
                description
                uuid
            }   
        }
    }
`
export const GET_FLOW = gql `
    query getFlow($id: uuid!){
        nodles_flow_runs(where: {uuid: {_eq: $id}}) {
            name
            flow
            description
        }
    }
`