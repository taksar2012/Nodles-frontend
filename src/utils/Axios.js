import axios from 'axios'

const instance = axios.create({
    baseURL: "https://apidev.altosphere.co"
})

export default instance