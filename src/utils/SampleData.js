export const JSONData = {
    "metadata": {
        "fields": [
            {
                "name": "sensor",
                "type": "string"
            },
            {
                "name": "start_date",
                "type": "datetime"
            },
            {
                "name": "num",
                "type": "number"
            },
            {
                "name": "count",
                "type": "integer"
            }
        ],
        "pandas_version": "0.20.0",
        "num_rows": 3,
        "num_cols": 4
    },
    "data": {
        "sensor": [
            "All",
            "Spd",
            "Dir",
            "Den",
            "Temp"
        ],
        "start_date": [
            "2016-01-09T15:30:00.000Z",
            "2016-03-09T06:20:00.000Z",
            "2016-07-09T06:20:00.000Z",
            "2016-04-09T06:20:00.000Z",
            "2016-08-09T06:20:00.000Z"
        ],
        "num": [
            3.0,
            2.0,
            17.6,
            5.0,
            9.1
        ],
        "count": [
            9,
            6,
            4,
            2,
            5
        ]
    }
}