import axios from 'axios'

let BASE_URL="https://upload.altosphere.co"
let GROUP_ID = process.env.REACT_APP_DOC_GROUP


export const uploadFile = async(file, token) => {
    const formData = new FormData()
    formData.append('file', file);

    let res = await axios({
        method: "post",
        url: `${BASE_URL}/groups/${GROUP_ID}/upload/`,
        data: formData,
        headers: {
            "Authorization": `Bearer ${token}`,
            "Content-Type": "multipart/form-data"
        },
    })
    return res
}

export const downloadFile = async(fileId, token) => {

    let res = await axios({
        method: "get",
        responseType: 'arraybuffer',
        url: `${BASE_URL}/groups/${GROUP_ID}/upload/${fileId}/preview`,
        headers: {
            "Authorization": `Bearer ${token}`
        },
    })

    return res

}

export const deleteFile = async(fileId, token) => {

    let res = await axios({
        method: "delete",
        url: `${BASE_URL}/groups/${GROUP_ID}/upload/${fileId}`,
        headers: {
            "Authorization": `Bearer ${token}`
        },
    })
    return res
}
