const nodes = [

    {
        "name": "input",
        "displayName": "Input Nodes",
        "type": "folder",
        "collections": [
            {
                "name": "textInput",
                "display_name": "Text Input",
                "type": "task",
                "nodeType": "textinput"
            },
            {
                "name": "dropdown",
                "display_name": "Drop Down",
                "type": "task",
                "nodeType": "dropdown"
            },
            {
                "name": "fileInput",
                "display_name": "File Input",
                "type": "task",
                "nodeType": "uploadedFile"
            }
        ]
    },
    {
        "name": "output",
        "displayName": "Output Nodes",
        "type": "folder",
        "collections": [
            {
                "name": "table",
                "display_name": "Table",
                "type": "task",
                "nodeType": "result"
            },
            {
                "name": "number",
                "display_name": "Number",
                "type": "task",
                "nodeType": "result"
            },
            {
                "name": "chart",
                "display_name": "Chart",
                "type": "task",
                "nodeType": "result"
            },
            {
                "name": "image",
                "display_name": "Image",
                "type": "task",
                "nodeType": "result"
            }
        ]
    },
    {
        "name": "notes",
        "display_name": "Notes",
        "type": "task",
        "nodeType": "note"
    }
]

export default nodes