export const getFormattedDate = (timeInMs = new Date().getTime())=>{
    const dateObj = new Date(timeInMs);

    return(
        dateObj.getDate()+ '-' + dateObj.getMonth()+ '-' + dateObj.getFullYear() + '  '+ dateObj.getHours()+ ":"+ dateObj.getMinutes()
    )
}