import { Backdrop, CircularProgress } from '@material-ui/core';
import React from 'react';

function Loader(){
    return(
        <Backdrop open={true} style={{background: 'rgba(0,0,0,.6)', zIndex: 99, color:'#fff'}} >
            <CircularProgress color={'inherit'} />
        </Backdrop>
    )
}

export default Loader;