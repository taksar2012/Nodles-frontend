import React, { useState, useEffect } from 'react'
import ClearIcon from '@material-ui/icons/Clear';
import "./Modals.css"

import RightIcon from "../.././assets/leftright/right.svg"
import LeftIcon from "../.././assets/leftright/left.svg"
import SearchIcon from "../.././assets/leftright/search.svg"
import UPIcon from "../.././assets/leftright/up.svg"
import DOWNIcon from "../.././assets/leftright/down.svg"

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'


const ResultModal = (props) => {

    const { setShowModal, data, type, id } = props

    const [page, setPage] = useState(1)
    const [start, setStart] = useState(0)
    const [end, setEnd] = useState(14)
    const [query, setQuery] = useState('')

    const [chart, setChart] = useState('line')
    const [xAxis, setXAxis] = useState('')
    const [yAxis, setYAxis] = useState('')


    useEffect(() => {

    }, [xAxis, yAxis])

    if (type === 'table') {
        const tbl = JSON.parse(data.outputs[id])
        // Table format changed. The output contains two fields now 'metadata' & 'data' 
        const tbContent = tbl.data
        // const tbContent = JSON.parse(data.outputs[id])
        const tbHeadings = Object.keys(tbContent)

        let allData = tbHeadings.map(t => (
            tbContent[t]
        ))

        let queryHeaders = tbHeadings.filter(h => {
            return h.toLowerCase().includes(query.toLowerCase());
        })

        let queryData = allData[0].map((a, i) => {
            let res = []
            queryHeaders.forEach(q => {
                res.push(tbContent[q][i])
            })
            return res
        })

        let pages = Math.ceil(queryData.length / 14)


        const increasePageNum = () => {
            if (page < pages) {
                setPage(page + 1)
                setStart(start + 14)
                setEnd(end + 14)
            }
        }

        const decreasePageNum = () => {
            if (page > 1) {
                setPage(page - 1)
                setStart(start - 14)
                setEnd(end - 14)
            }
        }

        return (
            <div className="res-modal">
                <div className="table">
                    <ClearIcon className="close-btn" onClick={() => setShowModal(false)} />
                    <div className="options">
                        <div className="search-data">
                            <img src={SearchIcon} alt="" />
                            <input type="search" placeholder="Search"
                                onChange={(e) => setQuery(e.target.value)} />
                        </div>

                        <div className="page-change">
                            <img src={LeftIcon} alt="" onClick={decreasePageNum} />
                            Page {page} / {pages}
                            <img src={RightIcon} alt="" onClick={increasePageNum} />
                        </div>
                    </div>
                    <div className="table-container">
                        <table>
                            <tr>
                                {queryHeaders.length > 0 ? queryHeaders.map(h => (
                                    <th>
                                        <div className="head">
                                            <p>{h}</p>
                                            <div className="sort-icons">
                                                <img src={UPIcon} alt="/" />
                                                <img src={DOWNIcon} alt="/" />
                                            </div>
                                        </div>
                                    </th>
                                )) : (
                                    <div className="no-data">
                                        <p>No data</p>
                                    </div>
                                )}
                            </tr>

                            {queryData.length > 0 && queryData.slice(start, end).map(r => (
                                <tr>
                                    {
                                        r.map(v => (
                                            <td>
                                                {v}
                                            </td>
                                        ))
                                    }
                                </tr>
                            ))}

                        </table>
                    </div>

                </div>
            </div>
        )

    }
    else {
        const chartContent = JSON.parse(data.outputs[id])

        let fields = chartContent.metadata.fields
        let values = chartContent.data

        // For date-time chart category

        let dt = fields.filter(f => f.type === 'datetime')[0]
        let datetimeCats = dt ? values[dt.name] : []

        // For scatter chart

        let initialCatY = fields.filter(f => f.type === 'number' || f.type === 'float')[0]
        let initialCatX = fields.filter(f => f.type === 'string' || f.type === 'datetime')[0]

        let curX = xAxis !== '' ? xAxis : initialCatX.name
        let curY = yAxis !== '' ? yAxis : initialCatY.name

        console.log(curY, curX, values[curX], values[curY])

        const chartCategories = values[curX]
        const chartValues = values[curY]

        const options = {
            chart: {
                height: 600,
                width: 900,
                borderColor: '#4A4A4A',
                borderWidth: 2,
                type: chart,
                marginBottom: 70
            },
            series: [{ data: chartValues }],
            yAxis: {
                lineColor: '#979797',
                lineWidth: 0.8,
                gridLineDashStyle: 'longdash',
                gridLineColor: '#515151',
            },
            xAxis: {
                categories: chart === 'line' ? datetimeCats : chartCategories,
                lineColor: '#979797',
                lineWidth: 0.8,
            },
        }

        console.log(chart, datetimeCats, chartCategories, chartValues)


        return (
            <div className="res-modal">
                <div className="chart-container">
                    <ClearIcon className="close-btn" onClick={() => setShowModal(false)} />
                    <div className="config-view">
                        <div className="heading">
                            Chart Configuration
                        </div>
                        <div className="divider"></div>
                        <div className="input-fields">
                            <div className="inputs">
                                <p>X Range</p>
                                <div className="fields">
                                    <input type="text" placeholder="min." />
                                    <input type="text" placeholder="max." />
                                </div>
                            </div>

                            <br />

                            <div className="inputs">
                                <p>Y Range</p>
                                <div className="fields">
                                    <input type="text" placeholder="min." />
                                    <input type="text" placeholder="max." />
                                </div>
                            </div>

                            <br />

                            <div className="inputs">
                                <p>X Label</p>
                                <div className="fields full-width">
                                    <input type="text" placeholder="X Axis Title" />
                                </div>
                            </div>

                            <br />

                            <div className="inputs">
                                <p>Y Label</p>
                                <div className="fields full-width">
                                    <input type="text" placeholder="Y Axis Title" />
                                </div>
                            </div>

                            <br />

                            <div className="inputs">
                                <p>Additional Configuration</p>
                                <div className="fields full-width">
                                    <input type="text" placeholder="Additional Config. here" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="chart-view">
                        <div className="options">
                            <div className="select">
                                Type:
                                <select onChange={(e) => setChart(e.target.value)}>
                                    <option value="line">Timeseries</option>
                                    <option value="scatter">Scatter</option>
                                    <option value="column">Histogram</option>
                                </select>
                            </div>
                            <div className="select">
                                X:
                                <select onChange={(e) => setXAxis(e.target.value)}>
                                    {chart === 'scatter' ? fields.filter(s => s.type !== 'number' && s.type !== 'float' && s.type !== 'integer').map(s => (
                                        <option value={s.name}>{s.name}</option>
                                    )) :
                                        chart === 'line' ?
                                            fields.filter(s => s.type === 'datetime')?.slice(0, 6).map(v =>
                                                <option value={v.name}>{v.name}</option>
                                            ) :
                                            fields.filter(s => s.type === 'string').slice(0, 6).map(v =>
                                                <option value={v.name}>{v.name}</option>
                                            )
                                    }
                                </select>
                            </div>
                            <div className="select">
                                Y:
                                <select onChange={(e) => setYAxis(e.target.value)}>
                                    {fields.filter(s => s.type === 'number' || s.type === 'float' || s.type === 'integer').map(v =>
                                        <option value={v.name}>{v.name}</option>
                                    )}
                                </select>
                            </div>

                        </div>

                        {chart === 'line' && datetimeCats.length !== 0 ?
                            <HighchartsReact
                                highcharts={Highcharts}
                                options={options}
                                className="highchart"
                            />
                            : chart !== 'line' && ( chartValues.length !== 0 || chartCategories.length ) !== 0 ? (
                                <HighchartsReact
                                    highcharts={Highcharts}
                                    options={options}
                                    className="highchart"
                                />
                            ) : <p style={{color: 'white'}}>No chart is available.</p>}

                    </div>
                </div>


            </div>
        )
    }


}

export default ResultModal
