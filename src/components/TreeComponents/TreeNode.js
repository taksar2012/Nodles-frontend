import React, { useState } from 'react'
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ClosedFolder from "../../assets/closedFolder/folder-closed.svg"
import BlockIcon from "../../assets/block/block.svg"
import OpenFolder from "../../assets/folder/shape.svg"

const TreeNode = (props) => {

    const [childVisible, setChildVisible] = useState(false)

    const hasChild = (
        props.node.collections
        && props.node.collections.length) ? true : false

    const isTask = props.node.type === 'task' ? true : false
    const isFolder = props.node.type === 'folder' ? true : false


    const onDragStart = (event) => {

        let willHaveTable = false;

        if (props.node.inputs !== undefined) {
            let tbNodes = props.node.inputs.filter(i => i.type === "table")
            if (tbNodes.length > 0) {
                willHaveTable = true;
            }
        }


        let typeOfNode = props.node.nodeType ? props.node.nodeType
            : "multiport"

        let FUN_NAME = typeOfNode === 'result' ? `show_${props.node.name}`
            : typeOfNode !== 'multiport' ? `input_${props.node.name}`
                : props.node.display_name

        let info = {
            name: props.node.display_name, /* Sending the displayname of each node */
            inputs: props.node.inputs, /* Sending the number of input ports required if exist */
            funName: FUN_NAME, /* This is the actual function name that will be needed when sending the graph to the backend */
            nodeType: typeOfNode, /* Sending node type */
            fun: props.node.fun !== undefined ? props.node.fun : null,
            funtype: props.node.fun !== undefined ? props.node.funtype : null,
            task_id: props.node.task_id !== undefined ? props.node.task_id : null,
            willHaveTable: willHaveTable
        }

        event.dataTransfer.setData('application/reactflow', JSON.stringify(info));
        event.dataTransfer.effectAllowed = 'move';
    };


    return (
        <>
            {(isTask || isFolder) && (

                <li className="tree-node" >
                    <div className={`doc-container ${isTask ? 'draggable' : ''}`}
                        onClick={() => setChildVisible(!childVisible)}
                        draggable={isTask}
                        onDragStart={(event) => onDragStart(event)}>

                        {!isTask && hasChild && (
                            <div className={`tree-toggler ${childVisible ? 'active' : ''}`}>
                                <ArrowRightIcon className="svg-icon" />
                            </div>
                        )}

                        <div className={`doc-label ${isTask ? 'task' :
                            !hasChild ? 'no-child' : ''}`
                        }>

                            {!childVisible ? (
                                <div className="icon">
                                    {props.node.type === 'folder' ? (
                                        <img src={ClosedFolder} alt="" />
                                    ) : (
                                        <img src={BlockIcon} alt="" />
                                    )}
                                </div>
                            ) : (
                                <div className="icon">
                                    {props.node.type === 'folder' ? (
                                        <img src={OpenFolder} alt="" />
                                    ) : (
                                        <img src={BlockIcon} alt="" />
                                    )}
                                </div>
                            )}
                            <p>{props.node.type === 'folder' ? props.node.displayName : props.node.display_name}</p>
                        </div>

                    </div>

                    {
                        !isTask && hasChild && childVisible && <div className="tree-content">
                            <ul>
                                {props.node.collections.map(n => (
                                    <div className="tree-content">
                                        <TreeNode node={n} key={n.name} />
                                    </div>

                                ))}
                            </ul>
                        </div>
                    }

                </li>

            )}

        </>
    )
}

export default TreeNode
