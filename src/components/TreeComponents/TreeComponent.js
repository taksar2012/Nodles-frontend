import React, { useState, useEffect } from 'react'
import "./TreeComponent.css"
import TreeNode from './TreeNode';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ClosedFolder from "../../assets/closedFolder/folder-closed.svg"
import OpenFolder from "../../assets/folder/shape.svg"
import SearchIcon from '@material-ui/icons/Search';
import BlockIcon from "../../assets/block/block.svg"



let list = []
const getBlockList = (array) => {

    array.forEach(a => {
        if (a.type === 'task') {
            list.push(a)
        }
        else if (a.type === 'folder') {
            getBlockList(a.collections)
        }
    })
    return list
}


const TreeComponent = (props) => {
    const { files } = props
    const data = files.collections

    const headNode = files.displayName
    const [childVisible, setChildVisible] = useState(false)
    const [blockList, setBlockList] = useState([])
    const [query, setQuery] = useState('')
    const hasChild = data.length ? true : false


    useEffect(() => {
        const l = getBlockList(data)
        setBlockList(l)
    }, [data])

    let searchList = blockList.filter(l => {
        return l.display_name.toLowerCase().includes(query.toLowerCase());
    })


    const onDragStart = (e,s) => {

        let willHaveTable = false;

        if (s.inputs !== undefined) {
            let tbNodes = s.inputs.filter(i => i.type === "table")
            if (tbNodes.length > 0) {
                willHaveTable = true;
            }
        }

        let typeOfNode = s.nodeType ? s.nodeType
        : "multiport"


        let FUN_NAME = s.nodeType === 'result' ? `show_${s.name}` 
                       :  s.nodeType !== 'multiport' ? `input_${s.name}`
                       : s.display_name

        let info = {
            name: s.display_name, /* Sending the displayname of each node */
            funName: FUN_NAME, /* This is the actual function name that will be needed when sending the graph to the backend */
            inputs: s.inputs, /* Sending the number of input ports required if exist */
            nodeType: typeOfNode, /* Sending node type */
            fun:s.fun !== undefined ? s.fun : null,
            funtype: s.fun !== undefined ? s.funtype : null,
            task_id: s.task_id !== undefined ? s.task_id : null,
            willHaveTable: willHaveTable
        }
        
        e.dataTransfer.setData('application/reactflow', JSON.stringify(info));
        e.dataTransfer.effectAllowed = 'move';
    };

    return (
        <div>

            <div className="search-field">
                <SearchIcon className="svg-icon" />
                <input type="search" placeholder="Search Block" onChange={(e) => setQuery(e.target.value)} value={query} />
            </div>
            <div className="search-res">
                {(query!=='') && searchList.map((s,i) => (
                    <div className="doc-container search-blocks"
                        key={i}
                        draggable={true}
                        onDragStart={(e) => onDragStart(e,s)}>
                        <div className="doc-label">
                            <div className="icon">
                                <img src={BlockIcon} alt="" />
                            </div>
                            <p>{s.display_name}</p>
                        </div>
                    </div>
                ))}
            </div>
            <div className="divider"></div>


            <div className="folders-container">

                <ul className="tree">
                    <div className="doc-container"
                        onClick={() => setChildVisible(!childVisible)}>

                        {hasChild && (
                            <div className={`tree-toggler ${childVisible ? 'active' : ''}`}>
                                <ArrowRightIcon className="svg-icon" />
                            </div>

                        )}

                        <div className="doc-label">

                            <div className="icon">
                                {!childVisible ?
                                    (<img src={ClosedFolder} alt="" />)
                                    : (<img src={OpenFolder} alt="" />)}
                            </div>
                            <p><strong>{headNode}</strong></p>
                        </div>

                    </div>

                    {
                        hasChild && childVisible && <div className="tree-content">
                            <ul className="content-list">
                                {data.map(n => (
                                    <div className="tree-content">
                                        <TreeNode node={n} key={n.name}/>
                                    </div>

                                ))}
                            </ul>
                        </div>
                    }

                </ul>
            </div>


            <div className="divider"></div>
        </div>
    )
}

export default TreeComponent
