import React, { useContext } from 'react'
import { AppContext } from '../../utils/AppContext'
import BlockIcon from "../../assets/block/block.svg"
import "./ShowSelected.css"
import uuid from 'react-uuid'


const ShowSelected = () => {

    const { setElements, selected, setSelected } = useContext(AppContext)

    const handleAdd = () => {
        selected.forEach(el => {
            el.id = uuid()
            let posY = el.position.y

            el.position.y = posY + 50
            setElements(n => [...n, el])
        });
        setSelected([])
    }

    const handleRemove = () => {
        setSelected([])
    }


    return (
        <div className="show-selected-container">
            <p className="ins">
                You can add blocks here by selecting them. After that you can duplicate all the blocks in the playground.
            </p>
            {selected.length ? selected.map(s => (
                <div className="doc-container selected-blocks" key={s.id}>
                    <div className="doc-label">
                        <div className="icon">
                            <img src={BlockIcon} alt="" />
                        </div>
                        <p>{s.data.label}</p>
                    </div>
                </div>
            )) : <p className="empty-message">No block selected</p>}

            {selected.length ? (
                <>
                    <button className="btn add-block-btn" onClick={handleAdd}>Add to playground</button>
                    <button className="btn clear-block-btn" onClick={handleRemove}>Clear</button>
                </>
            ) : ''}

        </div>
    )
}

export default ShowSelected
