import React, { useContext, useState, useRef, useEffect } from 'react'
import uuid from 'react-uuid'
import ReactFlow, { addEdge, removeElements, Controls, MiniMap, useStoreState, Background } from 'react-flow-renderer';
import { AppContext } from '../../utils/AppContext';
import MultiPortNode from '../CustomNodes/MultiPortNode';
import "./FlowChart.css"
import UserNotes from '../CustomNodes/UserNotes';
import Edge from '../CustomEdges/Edge';
import DropDown from '../CustomNodes/Input/DropDown';
import TextInputNode from "../CustomNodes/Input/TextInputNode"
import OutputNode from "../CustomNodes/Output/OutputNode"
import GroupNode from '../CustomNodes/GroupNode';
import TaskTable from "../CustomNodes/TaskTable/TaskTable"
import FileInput from '../CustomNodes/Input/FileInput';

const nodeTypes = {
    multiport: MultiPortNode,
    textinput: TextInputNode,
    uploadedFile: FileInput,
    note: UserNotes,
    dropdown: DropDown,
    result: OutputNode,
    group: GroupNode,
    multiportTbIp: TaskTable
};

const edgeTypes = {
    edge: Edge
}


const FlowChart = () => {

    const { elements, setElements, setRfInstance, resize, setCollection, collection } = useContext(AppContext)
    const reactFlowWrapper = useRef(null);
    const [reactFlowInstance, setReactFlowInstance] = useState(null);

    const state = useStoreState(state => state.nodes)
    const [nodeCol, setNodeCol] = useState([])

    useEffect(() => {
        if (collection.length > 1) {
            window.oncontextmenu = () => false
        }
        else {
            window.oncontextmenu = null
        }
    }, [collection])


    useEffect(() => {
        if (Object.keys(state).length > 0) {
            // let ar = state.map(s => s.__rf)
            // setNodes(state)
        }
    }, [state])


    // For connecting two nodes
    const onConnect = (params) => {
        const newEdge = {
            source: params.source,
            sourceHandle: params.sourceHandle,
            target: params.target,
            targetHandle: params.targetHandle,
            id: uuid(),
            type: 'edge',
            isHidden: false,
        }
        setElements((els) => addEdge(newEdge, els))
    };

    const onEdgeUpdate = (oldEdge, newEdge) => {
        console.log(oldEdge, newEdge)
    }


    // Drag & Drop functionality


    const onLoad = (_reactFlowInstance) => {
        setReactFlowInstance(_reactFlowInstance);
        setRfInstance(_reactFlowInstance)
    }

    const onDragOver = (event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
    };

    const onElementsRemove = (elementsToRemove) => {
        let res = window.confirm('Do you want to delete this node?')
        if (res) {
            setElements((els) => removeElements(elementsToRemove, els));
        }
    }


    var pane = document.getElementsByClassName("react-flow__renderer")[0];

    useEffect(() => {
        if (pane) {
            pane.onkeypress = (e) => {
            }
        }
    }, [pane])



    const onSelectionDragStop = (e, els) => {
        if (els) {
            let allNodes = els.filter(e => e.type !== 'edge')
            setCollection(allNodes)
            setNodeCol(els)
        }
    }



    const onSelectionChange = (els) => {
        if (els) {
            let allNodes = els.filter(e => e.type !== 'edge')
            setCollection(allNodes)
            setNodeCol(els)
        }
    }

    const onGroup = () => {

        const xPos = []
        const yPos = []
        collection.forEach((s, i) => {
            xPos.push(s.__rf.position.x)
            yPos.push(s.__rf.position.y)
        })
        yPos.sort()
        xPos.sort()

        // Getting the width of the group node
        let hlastEl = collection.filter(s => s.__rf.position.x === xPos[xPos.length - 1])
        const width = (xPos[xPos.length - 1] - xPos[0]) + hlastEl[0].__rf.width

        // Getting the height of the group node
        let vlastEl = collection.filter(s => s.__rf.position.y === yPos[yPos.length - 1])
        const height = (yPos[yPos.length - 1] - yPos[0]) + vlastEl[0].__rf.height

        let posDif = []
        collection.forEach(n => {
            let sn = Object.assign(n)
            let posObj = {
                x: (sn.__rf.position.x - xPos[0]),
                y: (sn.__rf.position.y - (yPos[0] - 40)),
            }
            posDif.push(posObj)
        })

        let groupNode = {
            id: uuid(),
            data: {
                label: 'Group', /* For multi port node */
                nodes: collection,
                allEls: nodeCol,
                style: {
                    minHeight: height + 40,
                    minWidth: width - 20
                },
                title: '',
                posDif: posDif
            },
            funName: 'Group Node',
            className: 'grpNd',
            type: 'group',
            position: { x: xPos[0], y: yPos[0] - 40 },
            style: {
                minHeight: height + 10,
                minWidth: width - 20,
                zIndex: -1
            },
        }

        // Adding group node to root elements

        setElements(el => [...el, groupNode])
        setCollection([])
    }


    const onDrop = (event) => {
        event.preventDefault();

        let newElems = elements

        const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
        const data = JSON.parse(event.dataTransfer.getData('application/reactflow'))

        const position = reactFlowInstance.project({
            x: event.clientX - reactFlowBounds.left,
            y: event.clientY - reactFlowBounds.top,
        });

        let nodeObj = {
            id: uuid(),
            data: {
                label: data.name, /* For multi port node */
                ports: data.inputs, /* For multi port node */
                value: '', /* For text input node */
                note: {}, /* For user notes */
                rows: [], /* For rows of table input  */
                output: 0 /* For output node  */
            },
            funName: data.funName,
            fun: data.fun,
            funtype: data.funtype,
            task_id: data.task_id,
            className: 'myNodes',
            type: data.nodeType,
            isHidden: false,
            position
        }

        newElems = [...newElems, nodeObj]

        if (data.willHaveTable) {
            const tbPos = reactFlowInstance.project({
                x: event.clientX - reactFlowBounds.left + 10,
                y: event.clientY - reactFlowBounds.top + 200,
            });

            let tbObj = {
                id: uuid(),
                parentID: nodeObj.id,
                data: {
                    label: "Input Table", /* For multi port node */
                    ports: data.inputs, /* For multi port node */
                    value: '', /* For text input node */
                    note: {}, /* For user notes */
                    rows: [], /* For rows of table input  */
                    output: 0 /* For output node  */
                },
                funName: data.funName,
                fun: data.fun,
                funtype: data.funtype,
                task_id: data.task_id,
                className: 'myNodes',
                type: 'multiportTbIp',
                isHidden: false,
                position: tbPos
            }

            newElems = [...newElems, tbObj]

            let edgeHandle = ''
            data.inputs.forEach(i => {
                if(i.type === 'table'){
                    edgeHandle = i.name;
                    // return i.name
                }
            })

            const tableBlockEdge = {
                source: tbObj.id,
                sourceHandle: "Input Table",
                target: nodeObj.id,
                targetHandle: edgeHandle,
                id: uuid(),
                type: 'edge',
                isHidden: false,
            }

            newElems = addEdge(tableBlockEdge, newElems)
            
        }

        


        // setElements((els) => addEdge(tableBlockEdge, els))
        setElements(newElems)
    };


    return (
        <div ref={reactFlowWrapper}
            className="reactflow-wrapper">

            <ReactFlow
                nodeTypes={nodeTypes}
                elements={elements}
                edgeTypes={edgeTypes}
                onConnect={onConnect}
                onDrop={onDrop}
                onDragOver={onDragOver}
                onLoad={onLoad}
                elementsSelectable={true}
                onElementsRemove={onElementsRemove}
                onSelectionContextMenu={onGroup}
                onEdgeUpdate={onEdgeUpdate}
                nodesDraggable={!resize}
                onSelectionChange={onSelectionChange}
                onSelectionDragStop={onSelectionDragStop}>

                <Background
                    variant="dots"
                    gap={12}
                    size={0.5}
                />

                <MiniMap
                    className="minimap-container"
                    nodeStrokeColor={(n) => {
                        if (n.type === 'multiport') return '#00ffff';

                        // For input nodes

                        if (n.type === 'textinput') return '#ff5e00';
                        if (n.type === 'note') return '#ffff00';
                        if (n.type === 'dropdown') return '#af0b4f';

                        // For output nodes

                        if (n.type === 'result') return '#7A4DFA';
                    }}
                    nodeColor={(n) => {
                        if (n.type === 'multiport') return '#00ffff';

                        // For input nodes

                        if (n.type === 'textinput') return '#ff5e00';
                        if (n.type === 'note') return '#ffff00';
                        if (n.type === 'dropdown') return '#af0b4f';

                        // For output nodes

                        if (n.type === 'result') return '#7A4DFA';
                    }}
                />
                <Controls className="zoom-controls">

                </Controls>
            </ReactFlow>


        </div>

    )
}

export default FlowChart


