import React, { useState, useContext, useEffect } from 'react'
import { AppContext } from '../../utils/AppContext';
import AppsIcon from '@material-ui/icons/Apps';
import { Handle } from 'react-flow-renderer';


const GroupNode = (props) => {
    const { data, id, xPos, yPos, isDragging } = props
    const { elements, setElements, setResize, resize } = useContext(AppContext)
    const [title, setTitle] = useState(data.title)
    const [collapse, setCollapse] = useState(false)
    const [menu, setMenu] = useState(false)
    const [ports, setPorts] = useState([])

    const allEls = data.allEls

    console.log(allEls)

    const allNdIds = allEls.filter(a => a.type !== 'edge').map(a => a.id)
    const outEdges = []
    const inEdges = []
    allEls.filter(a => a.type === 'edge').forEach(e => {
        if (!allNdIds.includes(e.source)) {
            inEdges.push(e)
        }
        if (!allNdIds.includes(e.target)) {
            outEdges.push(e)
        }
    })






    const rNode = document.getElementsByClassName('grpNd')[0]
    const btn = document.getElementById('resize-btn')

    useEffect(() => {
        data.title = title
    }, [title, data])

    // useEffect(() => {
    //     if(menu){
    //         actionMenu.focus()
    //     }
    // }, [menu])

    useEffect(() => {
        if (isDragging && !collapse) {
            data.nodes.forEach((n, i) => {
                let xp = data.posDif[i].x + xPos
                let yp = data.posDif[i].y + yPos

                let newEls = elements.map(el => {
                    if (el.id === n.id) {
                        let newPos = { x: xp, y: yp }
                        el.position = newPos
                    }
                    return el
                })

                setElements(newEls)
            })

        }

        if (isDragging && collapse) {
            data.nodes.forEach((n, i) => {
                let newPos = {
                    x: xPos + 10,
                    y: yPos + 10
                }

                let newEls = elements.map(el => {
                    if (el.id === n.id) {
                        el.position = newPos
                    }
                    return el
                })

                setElements(newEls)
            })

        }
    }, [xPos, yPos, isDragging, data.posDif, data.nodes, collapse, setElements, elements])


    useEffect(() => {

        var startX, startY, startWidth, startHeight;

        function initDrag(e) {
            setResize(true)
            startX = e.clientX;
            startY = e.clientY;
            startWidth = parseInt(document.defaultView.getComputedStyle(rNode).width, 10);
            startHeight = parseInt(document.defaultView.getComputedStyle(rNode).height, 10);
            if (resize) {
                document.documentElement.addEventListener('mousemove', doDrag, false);
                document.documentElement.addEventListener('mouseup', stopDrag, false);
            }

        }

        function doDrag(e) {
            rNode.style.width = (startWidth + e.clientX - startX) + 'px';
            rNode.style.height = (startHeight + e.clientY - startY) + 'px';
        }

        function stopDrag(e) {
            document.documentElement.removeEventListener('mousemove', doDrag, false);
            document.documentElement.removeEventListener('mouseup', stopDrag, false);
            setResize(false)
        }

        if (rNode && btn) {
            btn.addEventListener('mousedown', initDrag, false);
        }

    }, [rNode, resize, btn, setResize])

    const handleRemove = () => {
        let res = window.confirm('Do you want to ungroup elements?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            setElements(newElements)
            setMenu(false)
        }
    }

    const handleCollapse = () => {
        data.nodes.forEach(n => {
            let newPos = {
                x: xPos + 10,
                y: yPos - 20
            }
            let newEls = elements.map(el => {
                if (el.id === n.id) {
                    el.isHidden = true
                    el.position = newPos
                }
                return el
            })

            setElements(newEls)
        })
        setPorts([1, 2, 3, 4])
        setCollapse(true)
        setMenu(false)
    }

    const handleExpand = () => {

        data.nodes.forEach((n, i) => {
            let xp = data.posDif[i].x + xPos
            let yp = data.posDif[i].y + yPos
            let newPos = {
                x: xp,
                y: yp
            }
            let newEls = elements.map(el => {
                if (el.id === n.id) {
                    el.isHidden = false
                    el.position = newPos
                }
                return el
            })
            setPorts([])
            setElements(newEls)
        })

        setCollapse(false)
        setMenu(false)
    }

    window.addEventListener('click', (e) => {
        let x = document.getElementById('menu-btn')
        if (x !== e.target) {
            setMenu(false)
        }
    })


    return (
        <div className={`group ${collapse ? 'collapse' : ''} custom-node `} style={data.style}>
            {
                ports.map(p => (
                    <Handle type="source" position="right" />
                ))
            }
            <button id="resize-btn"></button>

            <div className={`controls ${collapse ? 'collapse' : ''}`}>
                <input className="title" placeholder="Title..." value={title ? title : ''}
                    onChange={(e) => setTitle(e.target.value)}></input>

                <div id="menu-btn-container" style={{ zIndex: '999' }}>
                    <AppsIcon className="menu" />
                    <div id="menu-btn" onClick={() => setMenu(!menu)} ></div>
                </div>


                <div className={`menu-list ${menu ? 'show' : ''}`}>
                    <ul>
                        <li onClick={handleRemove}>Ungroup</li>
                        <li>Create Component</li>
                        {
                            !collapse ? (
                                <li onClick={handleCollapse} >Collapse</li>
                            ) : (
                                <li onClick={handleExpand}>Expand</li>
                            )
                        }
                    </ul>
                </div>
            </div>

        </div>
    )
}

export default GroupNode
