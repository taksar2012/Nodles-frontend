import React, { useContext, useEffect, useState, useRef } from 'react'
import { Handle } from 'react-flow-renderer';
import "../NodeStyles.css"
import CancelIcon from '@material-ui/icons/Cancel';
import { AppContext } from '../../../utils/AppContext';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import ChartIcon from "../../../assets/nodes/chart.png"
import TableIcon from "../../../assets/nodes/table.png"
import ExpandIcon from "../../../assets/expand/expand.svg"
import ImageIcon from '@material-ui/icons/Image';
import CachedIcon from '@material-ui/icons/Cached';
import { JSONData } from "../../../utils/SampleData"

import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const SampleImg = 'https://images.unsplash.com/photo-1607543551362-9c1e4f4ac287?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1100&q=80'

const OutputNode = (props) => {

    const ndIcon = useRef()

    const { id, data, selected } = props
    const { elements, setElements, setSelected, setShowModal, response, wait, setONtype, setTbId } = useContext(AppContext)

    const nodeName = data.label.toLowerCase()

    const [content, setContent] = useState([])
    const [head, setHead] = useState([])
    const [chartCats, setChartCats] = useState([])
    const [dataSets, setDataSets] = useState([])
    const [allFields, setallFields] = useState([])
    const [chart, setChart] = useState('line')
    const [options, setOptions] = useState(null)
    const [xAxis, setXAxis] = useState('')
    const [yAxis, setYAxis] = useState('')
    const [empty, setEmpty] = useState(false)


    useEffect(() => {
        if (Object.keys(response).length > 0 && response.outputs[id]) {
            const tbl = JSON.parse(response.outputs[id])
            // Table format changed. The output contains two fields now 'metadata' & 'data' 

            if (nodeName === 'table') {
                const tbContent = tbl.data
                const tbHeadings = Object.keys(tbContent)

                let allData = tbHeadings.map(t => (
                    tbContent[t]
                ))
                let rowData = allData[0].map((a, i) => {
                    let res = []
                    tbHeadings.forEach(q => {
                        res.push(tbContent[q][i])
                    })
                    return res
                })

                setContent(rowData)
                setHead(tbHeadings)
            }

            else {

                setContent([tbl])

                console.log(tbl)

                let fields = tbl.metadata.fields
                let data = tbl.data
                console.log(fields, data)

                // For date-time chart category

                let dt = fields.filter(f => f.type === 'datetime')[0]
                let datetimeCats = dt ? data[dt.name] : []


                // For scatter chart

                let initialCatY = fields.filter(f => f.type === 'number' || f.type === 'float')[0]
                let initialCatX = fields.filter(f => f.type === 'string' || f.type === 'datetime')[0]

                let curX = xAxis !== '' ? xAxis : initialCatX.name
                let curY = yAxis !== '' ? yAxis : initialCatY.name

                // console.log(curY, curX, data[curX], data[curY])

                const chartCategories = data[curX]
                const chartValues = data[curY]

                setChartCats(chartCategories)
                setallFields(fields)

                if (chartValues.length === 0 || chartCategories.length === 0) {
                    setEmpty(true)
                }
                else if (chart === 'line' && datetimeCats.length === 0) {
                    setEmpty(true)
                }
                else {
                    setEmpty(false)
                }

                const opts = {
                    chart: {
                        height: 260,
                        width: 460,
                        type: chart,
                        margin: [20, 0, 20, 20]
                    },
                    series: [{ data: chartValues.slice(0, 5) }],
                    yAxis: {
                        lineColor: '#979797',
                        lineWidth: 0.8,
                        gridLineDashStyle: 'longdash',
                        gridLineColor: '#515151',
                    },
                    xAxis: {
                        categories: chart === 'line' ? datetimeCats : chartCategories.slice(0, 5),
                        lineColor: '#979797',
                        lineWidth: 0.8
                    },
                }
                setOptions(opts)
            }
        }
    }, [response, id, chart, xAxis, yAxis])

    // To remove nodes

    const handleRemove = (es) => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s, cloned])
    }

    const showTable = () => {
        setShowModal(true)
        setONtype('table')
        setTbId(id)
    }

    const showChart = () => {
        setShowModal(true)
        setONtype('chart')
        setTbId(id)
    }

    var nodeIcon = ndIcon.current

    useEffect(() => {
        if (nodeIcon) {
            nodeIcon.addEventListener('dragstart', function (e) {
                e.preventDefault();
            });
        }
    }, [nodeIcon])

    return (
        <>
            {
                (nodeName === 'number') ?
                    (
                        <div className={`num-node custom-node ot-nd ${selected ? 'sel-nd' : ''}`}>
                            <Handle type="target" id={`input_${data.label.toLowerCase()}`} position="left" />

                            {!wait ? (
                                <div>{data.output}</div>
                            ) : (<p style={{ opacity: '0.5' }}>Waiting for results...</p>)}

                            <CancelIcon className="cross-icon" onClick={handleRemove} />
                            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />
                        </div>
                    ) : (nodeName === 'table') ?
                        (
                            <div className={`table-node custom-node ot-nd ${selected ? 'sel-nd' : ''} ${content.length > 0 ? 'output' : ''}`}>

                                <Handle type="target" id={`input_${data.label.toLowerCase()}`} position="left" />
                                {
                                    content.length > 0 &&
                                    <p className="res-message">{content.length} Rows and {head.length} Columns obtained.</p>
                                }
                                {
                                    content.length > 0 && !wait ? (
                                        <img className="expand-svg" onClick={showTable} src={ExpandIcon} alt="" />
                                    ) : wait ? (
                                        <div className="waiting">
                                            <img ref={ndIcon} src={TableIcon} alt="icon" />
                                            <p>Waiting for results...</p>
                                        </div>
                                    ) : (
                                        <div>
                                            <img ref={ndIcon} src={TableIcon} alt="icon" />
                                            <button className="show-table pre">
                                                Run To See Table
                                            </button>
                                        </div>

                                    )
                                }

                                {
                                    content.length > 0 &&
                                    <div className="floating-window">
                                        <table className="prev-table">
                                            <tr>
                                                {head.slice(0, 2).map(h => (
                                                    <th>{h}</th>
                                                ))}
                                            </tr>

                                            {content.slice(0, 10).map(s => (
                                                <tr>
                                                    {s.slice(0, 2).map(data => (
                                                        <td>{data}</td>
                                                    ))}
                                                </tr>
                                            ))}
                                        </table>
                                        <p className="seeMore" onClick={showTable}>...See More</p>
                                    </div>
                                }

                                <CancelIcon className="cross-icon" onClick={handleRemove} />
                                <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />

                            </div>
                        ) : (nodeName === 'chart') ?
                            (
                                <div className={`chart-node custom-node ot-nd ${selected ? 'sel-nd' : ''} ${content.length > 0 ? 'output' : ''}`}>
                                    <Handle type="target" id={`input_${data.label.toLowerCase()}`} position="left" />

                                    {!wait && content.length > 0 ? (
                                        <div className="options">
                                            <div className="select">
                                                Type:
                                                <select onChange={(e) => setChart(e.target.value)}>
                                                    <option value="line">Timeseries</option>
                                                    <option value="scatter">Scatter</option>
                                                    <option value="column">Histogram</option>
                                                </select>
                                            </div>
                                            <div className="select">
                                                X:
                                                <select onChange={(e) => setXAxis(e.target.value)}>
                                                    {chart === 'scatter' ?
                                                        allFields.filter(s => s.type !== 'number' && s.type !== 'float' && s.type !== 'integer')
                                                            .slice(0, 6).map(s => (
                                                                <option value={s.name}>{s.name}</option>
                                                            )) :
                                                        chart === 'line' ?
                                                            allFields.filter(s => s.type === 'datetime')?.slice(0, 6).map(v =>
                                                                <option value={v.name}>{v.name}</option>
                                                            ) :
                                                            allFields.filter(s => s.type === 'string').slice(0, 6).map(v =>
                                                                <option value={v.name}>{v.name}</option>
                                                            )
                                                    }
                                                </select>
                                            </div>
                                            <div className="select">
                                                Y:
                                                <select onChange={(e) => setYAxis(e.target.value)}>
                                                    {allFields.filter(s => s.type === 'number' || s.type === 'float' || s.type === 'integer')
                                                        .slice(0, 6).map(v =>
                                                            <option value={v.name}>{v.name}</option>
                                                        )}
                                                </select>
                                            </div>
                                            <img className="expand-svg" onClick={showChart} src={ExpandIcon} alt="" />
                                        </div>
                                    ) : wait ? (
                                        <div className="waiting">
                                            <img src={ChartIcon} ref={ndIcon} alt="icon" />
                                            <p>Waiting for results...</p>
                                        </div>
                                    ) : (
                                        <div>
                                            <img src={ChartIcon} ref={ndIcon} alt="icon" />
                                            <button className="show-table pre">
                                                Run To See Chart
                                            </button>
                                        </div>
                                    )}

                                    {
                                        content.length > 0 && options !== null && empty !== true ?
                                            <div className="floating-window">
                                                <div className="chart-container">
                                                    <HighchartsReact
                                                        highcharts={Highcharts}
                                                        options={options}
                                                        className="highchart"
                                                    />
                                                </div>
                                            </div>
                                            : empty ? <p>No chart is available.</p>
                                                : null
                                    }


                                    <CancelIcon className="cross-icon" onClick={handleRemove} />
                                    <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />
                                </div>
                            ) : (
                                <div className={`image-node custom-node ot-nd ${selected ? 'sel-nd' : ''} output`}>

                                    <Handle type="target" position="right" id={`input_${data.label.toLowerCase()}`} />

                                    {
                                        content.length > 0 && !wait ? (
                                            <img src={SampleImg} ref={ndIcon} alt="" />
                                            // <img className="expand-svg" onClick={showTable} src={ExpandIcon} alt="" />
                                        ) : wait ? (
                                            <div className="waiting">
                                                <CachedIcon className="svg-icon waiting" />
                                                <p>Waiting for results...</p>
                                            </div>
                                        ) : (
                                            <div>
                                                <ImageIcon className="svg-icon" />
                                                <button className="show-table pre">
                                                    Run To See Image
                                                </button>
                                            </div>

                                        )
                                    }
                                    <CancelIcon className="cross-icon" onClick={handleRemove} />
                                    <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />
                                </div>
                            )
            }

        </>
    )
}

export default OutputNode