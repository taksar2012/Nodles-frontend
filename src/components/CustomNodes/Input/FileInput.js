import React, { useContext, useState, useEffect } from 'react'
import { Handle } from 'react-flow-renderer';
import "../NodeStyles.css"
import CancelIcon from '@material-ui/icons/Cancel';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import { AppContext } from '../../../utils/AppContext';
import { useRef } from 'react';
import PublishIcon from '@material-ui/icons/Publish';
import CircularProgress from '@material-ui/core/CircularProgress';
import DeleteIcon from '@material-ui/icons/Delete';
import { deleteFile, downloadFile, uploadFile } from '../../../utils/FileAPI';

const FileInput = (props) => {

    const fileInput = useRef(null)
    const PDF_IMAGE = "https://technofaq.org/wp-content/uploads/2019/04/pdf_pixabay_1493877090501.jpg"
    const DOC_IMAGE = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScSUs5CX6rXMVIFcr-YAQc6xlZ0mX_YKhq7U26ftaB9dWqtsev6GSAyoTvGCwZMUqfOgk&usqp=CAU"

    const { id, data, selected } = props

    const { elements, setElements, setSelected, setGroup, token, saveState } = useContext(AppContext)

    const [file, setFile] = useState(null)
    const [loading, setLoading] = useState(false)
    const [fs, setFs] = useState(false)
    const [fileId, setFileId] = useState(data.value)
    const [curUrl, setCurUrl] = useState(null)

    // To remove nodes

    const handleRemove = () => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s, cloned])
        setGroup(s => [...s, els[0]])
    }

    const handleChange = (e) => {
        e.preventDefault()
        let f = e.target.files[0]
        setFs(false)
        setFile(f)
    }

    const upload = () => {
        setLoading(true)
        uploadFile(file, token)
            .then(res => {
                setLoading(false)
                setFs(true)
                setFileId(res.data.upload_id)
                saveState()
            })
            .catch(err => {
                if (err) {
                    alert("Error occurred. Try again!")
                }
                setLoading(false)
            })
    }

    const remove = () => {
        setLoading(true)
        let fileId = data.value
        deleteFile(fileId, token)
            .then(res => {
                setLoading(false)
                setFs(false)
                setCurUrl(null)
                setFileId('')
                saveState()
            })
    }

    useEffect(() => {
        if (file) {
            getUrl(file)
        }
    }, [file])

    const getUrl = async (file) => {
        const reader = new FileReader()
        const returnURL = (url) => {

            if (file.type.includes('image')) {
                setCurUrl(url)
            }
            else if (file.type.includes('pdf')) {
                setCurUrl(PDF_IMAGE)
            }
            else {
                setCurUrl(DOC_IMAGE)
            }
        }
        reader.onload = () => {
            let blob = new Blob([reader.result], { type: file.type })
            let fileUrl = URL.createObjectURL(blob)
            returnURL(fileUrl)
        }
        reader.readAsArrayBuffer(file)
    }

    useEffect(() => {
        data.value = fileId
    }, [fileId])

    useEffect(() => {
        if (data && data.value !== '') {
            downloadFile(data.value)
                .then(res => {
                    let type = res.headers['content-type']
                    let blob = new Blob([res.data], { type: type.split('/')[1] })
                    var url = URL.createObjectURL(blob);
                    if(type.includes('image')){setCurUrl(url)}
                    else if(type.includes('pdf')){setCurUrl(PDF_IMAGE)}
                    else{setCurUrl(DOC_IMAGE)}
                    setFs(true)
                })
        }
    }, [data])


    return (
        <div className={`file-node custom-node ${selected ? 'sel-nd' : ''}`}>
            <Handle type="source" position="right" id={data.label} />
            <div className="input">
                {!file && !curUrl ? <p className="helper-text" style={{ marginBottom: '10px' }}>No file choosen.</p>
                    :
                    <div className={`file-area ${file && fs ? 'center' : ''}`}>
                        <img className="file-preview" src={curUrl} alt="/" />
                    </div>
                }
                <input placeholder="Input field"
                    type="file"
                    ref={fileInput}
                    value={""}
                    onChange={(e) => handleChange(e)}
                    style={{ display: 'none' }}></input>

                <div className="btn-container">
                    <button className="upload-btn" onClick={() => fileInput.current.click()}>
                        {!file ? 'Select File' : 'Select Again'}
                    </button>

                    {file && !fs && !loading && (
                        <button className="upload-btn" onClick={upload}>
                            Upload File
                        </button>
                    )}

                    {file && loading && !fs && (
                        <button className="upload-btn" onClick={upload}>
                            <CircularProgress thickness={5} size={16} disableShrink
                                className="pg-bar" />
                        </button>
                    )}

                    {fs && (
                        <button className="delete-btn" onClick={remove}>
                            Delete File
                        </button>
                    )}

                </div>

            </div>

            <CancelIcon className="cross-icon" onClick={handleRemove} />
            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />
        </div>
    )
}

export default FileInput
