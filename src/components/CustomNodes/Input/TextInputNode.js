import React, { useContext, useState, useEffect } from 'react'
import { Handle } from 'react-flow-renderer';
import "../NodeStyles.css"
import CancelIcon from '@material-ui/icons/Cancel';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import { AppContext } from '../../../utils/AppContext';

const TextInputNode = (props) => {

    const { id, data, selected } = props

    const { elements, setElements, setSelected, setGroup } = useContext(AppContext)
    const [label, setLabel] = useState(data.value)

    // To remove nodes

    const handleRemove = () => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s, cloned])
        setGroup(s => [...s,els[0]])
    }


    const handleInput = (e) => {
        setLabel(e.target.value)
    }

    useEffect(() => {
        data.value = label
    }, [label, data])


    return (
        <div className={`text-node custom-node ${selected ? 'sel-nd' : ''}`}>
            <Handle type="source" position="right" id={data.label} />
            <div className="input">
                <input placeholder="Input field" value={label}
                    onChange={(e) => handleInput(e)}></input>
            </div>



            <CancelIcon className="cross-icon" onClick={handleRemove} />
            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />
        </div>
    )
}

export default TextInputNode
