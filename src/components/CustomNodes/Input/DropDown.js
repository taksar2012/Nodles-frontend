import React, { useContext } from 'react'
import { Handle } from 'react-flow-renderer';
import "../NodeStyles.css"
import CancelIcon from '@material-ui/icons/Cancel';
import { AppContext } from '../../../utils/AppContext';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';

const DropDown = (props) => {

    const {id, data, selected } = props 
    const { elements, setElements, setSelected } = useContext(AppContext)

    // To remove nodes

    const handleRemove = (es) => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s,cloned])
    }


    return (
        <div className={`dropdown custom-node ${selected ? 'sel-nd' : '' }`}>
            <Handle type="source" position="right" id={data.label}/>
            <div className="select">
                <select name="options" id="options">
                    <option value="Option1">Option1</option>
                    <option value="Option2">Option2</option>
                    <option value="Option3">Option3</option>
                    <option value="Option4">Option4</option>
                </select>
            </div>

            <CancelIcon className="cross-icon" onClick={handleRemove} />
            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect}/>
        </div>
    )
}

export default DropDown
