import React, { useState, useEffect, useContext } from 'react'
import CancelIcon from '@material-ui/icons/Cancel';
import { AppContext } from '../../utils/AppContext';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';

const UserNotes = ({ data, id }) => {
    const { elements, setElements, setSelected } = useContext(AppContext)
    const [note, setNote] = useState(data.note)

    // To remove nodes

    const handleRemove = () => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s,cloned])
    }
    
    useEffect(() => {
        data.note = note
    }, [note, data])


    return (
        <div className="usernotes custom-node">
            <div className="inputs">
                <input className="title" placeholder="Title..." value={note ? note.title : ''}
                    onChange={(e) => setNote({...note, title: e.target.value})}></input>
                <textarea placeholder="Your notes..." value={note ? note.val : ''}
                    cols='20' rows='4'
                    onChange={(e) => setNote({...note, val: e.target.value})}></textarea>
            </div>

            <CancelIcon className="cross-icon" onClick={handleRemove} />
            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect}/>
        </div>
    )
}

export default UserNotes
