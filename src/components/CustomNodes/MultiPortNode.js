import React, { useContext } from 'react'
import { Handle } from 'react-flow-renderer';
import "./NodeStyles.css"
import CancelIcon from '@material-ui/icons/Cancel';
import { AppContext } from '../../utils/AppContext';
import AddCircleOutlinedIcon from '@material-ui/icons/AddCircleOutlined';
import BlockIcon from "../../assets/block/block.svg"

const MultiPortNode = (props) => {

    const { id, data, selected } = props

    const { elements, setElements, setSelected } = useContext(AppContext)

    let cols = []
    let tablePorts = []
    data.ports.forEach(i => {
        if (i.type === "table") {
            i.columns.forEach(c => {
                cols.push(c)
            })
            tablePorts.push(i)
        }
    })

    // To remove nodes

    const handleRemove = (es) => {
        let res = window.confirm('Do you want to delete this node?')

        if (res) {
            let newElements = elements.filter(e => e.id !== id)
            newElements = newElements.filter(e => e.source !== id)
            newElements = newElements.filter(e => e.target !== id)
            if(tablePorts.length > 0){
                newElements = newElements.filter(e => e.parentID !== id)
            }
            setElements(newElements)
        }
    }

    const handleSelect = () => {
        let els = elements.filter(e => e.id === id)
        const cloned = {}
        Object.assign(cloned, els[0])
        setSelected(s => [...s, cloned])
    }


    return (
        <div className={`mul-node custom-node ${selected ? 'sel-nd' : ''}`}>
            <Handle type="source" position="right" id={`${data.label}`} />


            {/* For connecting table node */}

            {tablePorts.length > 0 && tablePorts.map((p, i) => {
                return (
                    <Handle
                        key={i}
                        type="target"
                        id={p.name}
                        className="offset-table-port"
                        position='bottom'
                    >
                        <div className="port-name"><p>{p.name}</p></div>
                    </Handle>
                )

            })}

            <div id="label"><span><img src={BlockIcon} style={{ margin: '0 8px -2px 0' }} alt="" /></span>{data.label}</div>

            <CancelIcon className="cross-icon" onClick={handleRemove} />


            {/* For connecting others node */}

            <div className={`tg-handle-container ${data.ports.length > 2 ? 'exceed' : ''}`}>
                {data.ports && data.ports.map((p, i) => {
                    if (p.type !== 'table') {
                        return (
                            <Handle
                                key={i}
                                type="target"
                                id={p.name}
                                className="custom-target"
                                position={`${data.ports.length > 2 ? 'bottom' : 'left'}`}
                            >
                                <div className="port-name"><p>{p.name}</p></div>
                            </Handle>
                        )
                    }
                })}
            </div>

            <AddCircleOutlinedIcon className="select-icon" onClick={handleSelect} />

        </div>
    )
}

export default MultiPortNode
