import React, { useState, useEffect } from 'react'
import { Handle } from 'react-flow-renderer';
import "../NodeStyles.css"
import DatePicker from 'react-datepicker'
import uuid from 'react-uuid'
import TableRow from './TableRow';

const TableInput = (props) => {

    const { data, selected } = props

    const cols = data.ports.filter(i => i.type === "table").map(i => i.columns)[0]

    const [rowData, setRowData] = useState({})
    const [rows, setRows] = useState(data.rows)
    const [dateVal, setDateVal] = useState('')
    const [keyOrders, setKeyOrders] = useState([])

    useEffect(() => {
        if (Object.keys(rowData).length === 0) {
            let nD = rowData
            let kO = []
            cols.forEach(c => {
                nD[c.name] = ''
                kO.push(c.name)
            })
            setRowData(nD)
            setKeyOrders(kO)
            setDateVal('')
        }
    }, [rowData])


    const addRow = () => {
        let newRows = rows
        rowData['id'] = uuid()
        newRows.push(rowData)
        setRows(newRows)
        setRowData({})
    }

    const handleOnChange = (e, c) => {
        let value = e.target.value;
        let prop = c.name
        let newD = rowData
        newD = { ...newD, [prop]: value }
        setRowData(newD)
    }

    const handleOnChangeDate = (date, c) => {
        let prop = c.name
        let newD = rowData
        let d = `${new Date(date).getDate()}-${new Date(date).getMonth() + 1}-${new Date(date).getFullYear()}`
        newD = { ...newD, [prop]: d }
        setRowData(newD)
        setDateVal(date)
    }

    useEffect(() => {
        data.rows = rows
        data.values = rows
    }, [rows.length, data, rows])


    return (
        <div className={`table-input-node custom-node ${selected ? 'sel-nd' : ''}`}>
            <Handle type="source" position="top" id={data.label} style={{ top: '-10px' }} />

            <div className="table-input">
                <table>
                    {
                        rows.map((r, i) => (
                            <TableRow
                                r={r}
                                setRows={setRows}
                                rows={rows} 
                                order={keyOrders}/>
                        ))
                    }
                    <tr>
                        {cols.map(c => (
                            <td>
                                {
                                    c.type !== 'datetime' ? (
                                        <input
                                            placeholder={`${c.name}`}
                                            value={rowData[c.name]}
                                            onChange={e => handleOnChange(e, c)}
                                            type={c.type === 'str' ? 'text' : 'number'} />
                                    ) : (
                                        <div className="last-row">
                                            <DatePicker
                                                placeholderText={`${c.name}`}
                                                dateFormat="dd/MM/yyyy"
                                                closeOnScroll={true}
                                                selected={dateVal}
                                                onChange={(date) => handleOnChangeDate(date, c)} />
                                        </div>
                                    )
                                }
                            </td>
                        ))}
                    </tr>
                </table>
                <button className="add-btn" onClick={(e) => addRow(e)}>+ Add Row</button>
            </div>
        </div>
    )
}

export default TableInput
