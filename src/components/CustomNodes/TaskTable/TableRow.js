import React, { useState, useRef } from 'react'
import MoreVertIcon from '@material-ui/icons/MoreVert';

const TableRow = ({ r, setRows, rows, order }) => {

    const moreBtn = useRef(null)

    const [menu, setMenu] = useState(false)

    const handleDelete = (id) => {
        let newRows = rows.filter(r => r.id !== id)
        setRows(newRows)
    }

    window.addEventListener('click', (e) => {
        let x = moreBtn.current
        if (x !== e.target) {
            setMenu(false)
        }
    })


    return (
        <tr>
            {order.map((key, i) => {
                if (i !== order.length - 1) {
                    return (
                        <td>
                            <input value={r[key]} />
                        </td>
                    )
                }
                else {
                    return (
                        <td>
                            <div className="last-row">
                                <input value={r[key]} />
                                <div className="controls">
                                    <MoreVertIcon className="svg-icon" />
                                    <div id="control-btn" ref={moreBtn} onClick={() => setMenu(!menu)}></div>
                                    {menu && (
                                        <div className="more-menu">
                                            <ul>
                                                <li onClick={() => handleDelete(r.id)}>Delete row</li>
                                            </ul>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </td>
                    )
                }
            })}
        </tr>

    )
}

export default TableRow
