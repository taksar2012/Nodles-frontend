import React, { useContext } from 'react';
import { getSmoothStepPath, getMarkerEnd } from 'react-flow-renderer';
import { AppContext } from '../../utils/AppContext';
import "./Edge.css"

export default function Edge(props) {
    const {
        id,
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourcePosition,
        targetPosition,
        style = {},
        arrowHeadType,
        markerEndId,
        centerX,
        centerY,
        sourceHandleId
    } = props

    const edgePath = getSmoothStepPath({
        sourceX,
        sourceY,
        sourcePosition,
        targetX,
        targetY,
        targetPosition,
        centerX,
        centerY
    });
    const markerEnd = getMarkerEnd(arrowHeadType, markerEndId);

    const { elements, setElements, run } = useContext(AppContext)

    const handleRemove = () => {
        if (sourceHandleId !== 'Input Table') {
            const newElements = elements.filter(e => e.id !== id)
            setElements(newElements)
        }
    }

    const isInput = sourceHandleId === 'Text Input'
        || sourceHandleId === 'Drop Down'
        || sourceHandleId === 'Input Table' ? true : false

    return (
        <>
            <g className={`react-flow__edge react-flow__edge-edge ${run ? 'animated' : ''}`}>

                <path id={id} style={style} className={`react-flow__edge-path ${run ? 'running' : ''} ${isInput ? 'it-edge' : ''}`} d={edgePath} markerEnd={markerEnd}
                    onClick={handleRemove} />

                {!isInput && targetPosition === 'left' ? (
                    <polyline points={`${targetX - 16} ${targetY + 5}, ${targetX - 10} ${targetY}, ${targetX - 16} ${targetY - 5}`}
                        className={`arrow ${isInput ? 'it-arrow' : ''}`} />
                ) : !isInput ? (
                    <polyline points={`${targetX - 5} ${targetY + 16}, ${targetX} ${targetY + 10}, ${targetX + 5} ${targetY + 16}`}
                        className={`arrow ${isInput ? 'it-arrow' : ''}`} />
                ) : ''}
            </g>
        </>
    );
}