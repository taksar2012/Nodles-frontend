import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css"
import Dashboard from './views/Dashboard/Dashboard'
import Home from './views/Home/Home';
import {
    ApolloClient,
    ApolloProvider,
    InMemoryCache,
    HttpLink,
    from
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';

const GRAPHQL_SECRET = 'RAPT2021!';
const GRAPH_QL_END_POINT = 'https://ql.altosphere.co/v1/graphql';
const errorLink = onError(({ graphqlErrors, networkErrors }) => {
    if (graphqlErrors) {
        graphqlErrors.map(({ message, location, path }) => {
            console.error('GraphQL Error ', message);
        })
    }
})

const link = from([
    errorLink,
    new HttpLink({
        uri: GRAPH_QL_END_POINT, 
        headers: {
            'x-hasura-admin-secret': GRAPHQL_SECRET
        }
    })
])

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: link
})
const App = () => {
    return (
        <ApolloProvider client={client} >
            <Router>
                <div>
                    <Switch>
                        <Route path="/nodles/flow/:id" component={Dashboard} />
                        <Route path="/nodles/" component={Home} />
                    </Switch>
                </div>
            </Router>
        </ApolloProvider>

    )
}

export default App
